% Generated by roxygen2: do not edit by hand
% Please edit documentation in R/unit_indicators.R
\name{unit_indicators}
\alias{unit_indicators}
\title{Calculate Proportions Per Sub-Unit}
\usage{
unit_indicators(df, unitvar = "mere.hosp.uid",
  denominator = "bebe.alivelab", numerator = "ind.admis.nicu")
}
\arguments{
\item{df}{Working data set (in data.frame format)}

\item{unitvar}{This is the variable which describes the clusters (e.g. hospitals in which the individual patients (rows) are located).}

\item{numerator}{The variable used as the numerator (needs to be binary, coded as 0 or 1).}

\item{demoninator}{The variable used as the denominator (needs to be binary, coded as 0 or 1).}
}
\description{
This function calculates proportions per cluster unit contained within a data set.
}
\examples{
unit_indicators(df, unitvar="mere.hosp.uid", denominator="bebe.alivelab", numerator="ind.admis.nicu")
}
\keyword{meta-analysis}
\keyword{proportion}
\keyword{variance}
